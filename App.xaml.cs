﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using PInvoke;
using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using SharpDX.IO;
using SharpDX.Mathematics.Interop;
using SharpDX.WIC;
using ControlStyles = System.Windows.Forms.ControlStyles;

namespace WallpaperEngine
{
    public static class MouseHook
    {
        public static event EventHandler<MouseMoveArgs> MouseMove = delegate (object sender, MouseMoveArgs args) { };

        public class MouseMoveArgs
        {
            public MouseMoveArgs(POINT pos) { Pos = pos; }
            public POINT Pos { get; }
        }

        public static void Start()
        {
            _hookID = SetHook(_proc);
        }
        public static void Stop()
        {
            UnhookWindowsHookEx(_hookID);
        }

        private static LowLevelMouseProc _proc = HookCallback;
        private static IntPtr _hookID = IntPtr.Zero;

        private static IntPtr SetHook(LowLevelMouseProc proc)
        {
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess.MainModule)
            {
                return SetWindowsHookEx(WH_MOUSE_LL, proc,
                  GetModuleHandle(curModule.ModuleName), 0);
            }
        }

        private delegate IntPtr LowLevelMouseProc(int nCode, IntPtr wParam, IntPtr lParam);

        private static IntPtr HookCallback(
          int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode >= 0 && MouseMessages.WM_MOUSEMOVE == (MouseMessages)wParam)
            {
                MSLLHOOKSTRUCT hookStruct = (MSLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(MSLLHOOKSTRUCT));
                MouseMove(null, new MouseMoveArgs(hookStruct.pt));
            }
            return CallNextHookEx(_hookID, nCode, wParam, lParam);
        }

        private const int WH_MOUSE_LL = 14;

        private enum MouseMessages
        {
            WM_LBUTTONDOWN = 0x0201,
            WM_LBUTTONUP = 0x0202,
            WM_MOUSEMOVE = 0x0200,
            WM_MOUSEWHEEL = 0x020A,
            WM_RBUTTONDOWN = 0x0204,
            WM_RBUTTONUP = 0x0205
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int x;
            public int y;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct MSLLHOOKSTRUCT
        {
            public POINT pt;
            public uint mouseData;
            public uint flags;
            public uint time;
            public IntPtr dwExtraInfo;
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelMouseProc lpfn, IntPtr hMod, uint dwThreadId);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]

        private static extern IntPtr GetModuleHandle(string lpModuleName);
    }

    public partial class FgForm : System.Windows.Forms.Form
    {
        [DllImport("dwmapi.dll")]
        static extern void DwmExtendFrameIntoClientArea(IntPtr hWnd, ref Margins pMargins);

        internal struct Margins
        {
            public int Left, Right, Top, Bottom;
        }

        public FgForm()
        {
            this.TopMost = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Width = (int)SystemParameters.PrimaryScreenWidth;
            this.Height = (int)SystemParameters.PrimaryScreenHeight;
            this.BackColor = System.Drawing.Color.Black;
            this.Visible = true;
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer |
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.DoubleBuffer |
                ControlStyles.UserPaint |
                ControlStyles.Opaque |
                ControlStyles.ResizeRedraw |
                ControlStyles.SupportsTransparentBackColor, true);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.DoubleBuffered = true;
            this.TransparencyKey = System.Drawing.Color.Black;

            Margins marg;
            marg.Left = 0;
            marg.Top = 0;
            marg.Right = this.Width;
            marg.Bottom = this.Height;

            DwmExtendFrameIntoClientArea(this.Handle, ref marg);
        }

        protected override System.Windows.Forms.CreateParams CreateParams
        {
            get
            {
                System.Windows.Forms.CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x20 | 0x80000 | 0x80;
                return cp;
            }
        }
    }

    public partial class App : System.Windows.Application
    {
        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr SendMessageTimeout(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam, User32.SendMessageTimeoutFlags flags, int timeout, out IntPtr pdwResult);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);
        [DllImport("user32.dll")]
        static extern bool InvalidateRect(IntPtr hWnd, IntPtr lpRect, bool bErase);
        [DllImport("user32.dll")]
        static extern bool EnumWindows(EnumWindowsProc enumProc, IntPtr lParam);
        [DllImport("dwmapi.dll")]
        static extern int DwmGetWindowAttribute(IntPtr hwnd, int dwAttribute, out RECT pvAttribute, int cbAttribute);
        [DllImport("dwmapi.dll")]
        static extern int DwmGetWindowAttribute(IntPtr hwnd, int dwAttribute, out bool pvAttribute, int cbAttribute);

        public delegate bool EnumWindowsProc(IntPtr hWnd, IntPtr lParam);


        public IntPtr workerw = IntPtr.Zero;
        public IntPtr progman;

        public float mouseX = 0;

        public SharpDX.Direct3D11.Device1 device;
        public SharpDX.Direct2D1.DeviceContext bgContext;
        public SharpDX.Direct2D1.Device bgDevice;
        public SwapChain1 bgSwapChain;
        public Bitmap1 bgTarget;
        public WindowRenderTarget fgTarget;
        public CancellationTokenSource cancelRendering;
        public System.Windows.Forms.Form bg;
        public System.Windows.Forms.Form fg;

        public IntPtr fgHandle;
        public IntPtr nvOverlay;
        public IntPtr nvOverlay2;
        public IntPtr shellWindow;

        public App()
        {
            progman = User32.FindWindow("Progman", null);
            IntPtr result = IntPtr.Zero;

            SendMessageTimeout(progman,
                       0x052C,
                       new IntPtr(0),
                       IntPtr.Zero,
                       User32.SendMessageTimeoutFlags.SMTO_NORMAL,
                       1000,
                       out result);

            User32.EnumWindows(new User32.WNDENUMPROC((tophandle, topparamhandle) =>
            {
                IntPtr p = User32.FindWindowEx(tophandle,
                                            IntPtr.Zero,
                                            "SHELLDLL_DefView",
                                           "");
                if (p != IntPtr.Zero)
                {
                    this.workerw = User32.FindWindowEx(IntPtr.Zero,
                                               tophandle,
                                               "WorkerW",
                                               "");
                }
                return true;
            }), IntPtr.Zero);

            bg = new System.Windows.Forms.Form();
            bg.Width = (int)SystemParameters.PrimaryScreenWidth;
            bg.Height = (int)SystemParameters.PrimaryScreenHeight;
            bg.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

            fg = new FgForm();

            User32.SetParent(bg.Handle, workerw);

            SharpDX.Direct3D11.Device defaultDevice = new SharpDX.Direct3D11.Device(DriverType.Hardware, DeviceCreationFlags.Debug | DeviceCreationFlags.BgraSupport);
            device = defaultDevice.QueryInterface<SharpDX.Direct3D11.Device1>();

            SharpDX.DXGI.Device2 dxgiDevice2 = device.QueryInterface<SharpDX.DXGI.Device2>();
            Adapter dxgiAdapter = dxgiDevice2.Adapter;
            SharpDX.DXGI.Factory2 dxgiFactory2 = dxgiAdapter.GetParent<SharpDX.DXGI.Factory2>();

            SwapChainDescription1 description = new SwapChainDescription1()
            {
                Width = 0,
                Height = 0,
                Format = Format.B8G8R8A8_UNorm,
                Stereo = false,
                SampleDescription = new SampleDescription(1, 0),
                Usage = Usage.RenderTargetOutput,
                BufferCount = 2,
                Scaling = Scaling.None,
                SwapEffect = SwapEffect.FlipSequential,
            };

            bgSwapChain = new SwapChain1(dxgiFactory2, device, bg.Handle, ref description);
            var fgSwapChain = new SwapChain1(dxgiFactory2, device, fg.Handle, ref description);

            SharpDX.Direct2D1.Factory d2dFactory = new SharpDX.Direct2D1.Factory(FactoryType.SingleThreaded);

            BitmapProperties1 properties = new BitmapProperties1(new SharpDX.Direct2D1.PixelFormat(Format.B8G8R8A8_UNorm, SharpDX.Direct2D1.AlphaMode.Premultiplied), d2dFactory.DesktopDpi.Width, d2dFactory.DesktopDpi.Height, BitmapOptions.Target | BitmapOptions.CannotDraw);

            bgDevice = new SharpDX.Direct2D1.Device(dxgiDevice2);
            bgContext = new SharpDX.Direct2D1.DeviceContext(bgDevice, DeviceContextOptions.None);
            Surface bgBackBuffer = bgSwapChain.GetBackBuffer<Surface>(0);
            bgTarget = new Bitmap1(bgContext, bgBackBuffer, properties);
            bgContext.Target = bgTarget;

            fgTarget = new WindowRenderTarget(d2dFactory, new RenderTargetProperties
            {
                PixelFormat = new SharpDX.Direct2D1.PixelFormat(Format.B8G8R8A8_UNorm, SharpDX.Direct2D1.AlphaMode.Premultiplied),
            }, new HwndRenderTargetProperties
            {
                Hwnd = fg.Handle,
                PixelSize = new Size2(fg.Width, bg.Height),
                PresentOptions = PresentOptions.Immediately,
            });

            Bitmap1 farTreesBitmap = LoadBitmapFromContentFile(bgContext, ".\\Images\\FarTrees.png");
            BitmapBrush1 farTreesBrush = new BitmapBrush1(bgContext, farTreesBitmap, new BitmapBrushProperties1()
            {
                ExtendModeX = ExtendMode.Wrap,
                ExtendModeY = ExtendMode.Wrap,
            });

            Bitmap1 bgTreesBitmap = LoadBitmapFromContentFile(bgContext, ".\\Images\\BgTrees.png");
            BitmapBrush1 bgTreesBrush = new BitmapBrush1(bgContext, bgTreesBitmap, new BitmapBrushProperties1()
            {
                ExtendModeX = ExtendMode.Wrap,
                ExtendModeY = ExtendMode.Wrap,
            });

            Bitmap1 groundBitmap = LoadBitmapFromContentFile(bgContext, ".\\Images\\Ground.png");
            BitmapBrush1 groundBrush = new BitmapBrush1(bgContext, groundBitmap, new BitmapBrushProperties1()
            {
                ExtendModeX = ExtendMode.Wrap,
                ExtendModeY = ExtendMode.Wrap,
            });
            Bitmap1 foregroundBitmap = LoadBitmapFromContentFile(bgContext, ".\\Images\\Foreground.png");
            BitmapBrush1 foregroundBrush = new BitmapBrush1(bgContext, foregroundBitmap, new BitmapBrushProperties1()
            {
                ExtendModeX = ExtendMode.Wrap,
                ExtendModeY = ExtendMode.Wrap,
            });

            SharpDX.Direct2D1.Bitmap decorationBitmap = LoadBitmapFromContentFile(fgTarget, ".\\Images\\Decoration.png");
            BitmapBrush decorationBrush = new BitmapBrush(fgTarget, decorationBitmap, new BitmapBrushProperties()
            {
                ExtendModeX = ExtendMode.Wrap,
                ExtendModeY = ExtendMode.Wrap,
            });


            SolidColorBrush clearBrush = new SolidColorBrush(fgTarget, Color.Black);

            int width = bgSwapChain.Description.ModeDescription.Width;
            int height = bgSwapChain.Description.ModeDescription.Height;
            float scale = (float)height / foregroundBitmap.PixelSize.Height;
            float decorationScale = 40F / (float)decorationBitmap.PixelSize.Height;

            cancelRendering = new CancellationTokenSource();
            CancellationToken token = cancelRendering.Token;

            Task.Run(() =>
            {
                while (true)
                {
                    if (token.IsCancellationRequested)
                    {
                        token.ThrowIfCancellationRequested();
                    }

                    bgContext.BeginDraw();

                    farTreesBrush.Transform = Matrix3x2.Scaling(scale);
                    bgContext.FillRectangle(new RawRectangleF(0, 0, width, height), farTreesBrush);

                    bgTreesBrush.Transform = Matrix3x2.Scaling(scale) * Matrix3x2.Translation(-mouseX * 0.1F, 0);
                    bgContext.FillRectangle(new RawRectangleF(0, 0, width, height), bgTreesBrush);

                    groundBrush.Transform = Matrix3x2.Scaling(scale) * Matrix3x2.Translation(-mouseX * 0.2F, 0);
                    bgContext.FillRectangle(new RawRectangleF(0, 0, width, height), groundBrush);

                    foregroundBrush.Transform = Matrix3x2.Scaling(scale) * Matrix3x2.Translation(-mouseX * 0.4F, 0);
                    bgContext.FillRectangle(new RawRectangleF(0, 0, width, height), foregroundBrush);

                    bgContext.EndDraw();

                    bgSwapChain.Present(1, PresentFlags.None);
                }
            });

            shellWindow = User32.GetShellWindow();
            nvOverlay = User32.FindWindow("CEF-OSC-WIDGET", "NVIDIA GeForce Overlay");
            nvOverlay2 = User32.FindWindow("CEF-OSC-WIDGET", "NVIDIA GeForce Overlay DT");
            fgHandle = fg.Handle;

            Task.Run(() =>
            {
                while (true)
                {
                    if (token.IsCancellationRequested)
                    {
                        token.ThrowIfCancellationRequested();
                    }

                    fgTarget.BeginDraw();
                    fgTarget.Clear(Color.Transparent);

                    List<RECT> wds = GetWindows();

                    wds.Reverse();
                    wds.ForEach((rect) =>
                    {
                        RawRectangleF rect3 = new RawRectangleF(rect.left, rect.top, rect.right, rect.bottom);
                        RawRectangleF rect2 = new RawRectangleF(rect.left, rect.top, rect.right, rect.top - 40F);

                        decorationBrush.Transform = Matrix3x2.Scaling(decorationScale) * Matrix3x2.Translation(rect2.Left, rect2.Bottom);

                        fgTarget.FillRectangle(rect2, decorationBrush);
                        fgTarget.FillRectangle(rect3, clearBrush);
                    });

                    fgTarget.Flush();
                    fgTarget.EndDraw();
                }
            });

            MouseHook.Start();
            MouseHook.MouseMove += MouseMove;

            fg.Show();
            bg.Show();
        }

        private List<RECT> GetWindows()
        {
            List<RECT> wds = new List<RECT>();

            EnumWindows(delegate (IntPtr wnd, IntPtr param)
            {
                if (User32.GetWindowTextLength(wnd) == 0)
                {
                    return true;
                }

                IntPtr owner = User32.GetWindow(wnd, User32.GetWindowCommands.GW_OWNER);
                int style = User32.GetWindowLong(wnd, User32.WindowLongIndexFlags.GWL_EXSTYLE);

                if (owner != IntPtr.Zero && (style & 0x00040000L) != 0)
                {
                    return true;
                }

                if (wnd == fgHandle || wnd == workerw || wnd == shellWindow || wnd == nvOverlay || wnd == nvOverlay2)
                {
                    return true;
                }

                if (!User32.IsWindowVisible(wnd))
                {
                    return true;
                }


                DwmGetWindowAttribute(wnd, (int)DwmApi.DWMWINDOWATTRIBUTE.DWMWA_CLOAKED, out bool isCloacked, Marshal.SizeOf(typeof(bool)));

                if (isCloacked)
                {
                    return true;
                }

                if (User32.IsIconic(wnd))
                {
                    return true;
                }

                RECT rect = new RECT();
                DwmGetWindowAttribute(wnd, (int)DwmApi.DWMWINDOWATTRIBUTE.DWMWA_EXTENDED_FRAME_BOUNDS, out rect, Marshal.SizeOf(typeof(RECT)));

                wds.Add(rect);

                return true;
            }, IntPtr.Zero);

            return wds;
        }

        private Bitmap1 LoadBitmapFromContentFile(SharpDX.Direct2D1.DeviceContext context, string filePath)
        {
            Bitmap1 newBitmap;

            ImagingFactory imagingFactory = new ImagingFactory();
            NativeFileStream fileStream = new NativeFileStream(filePath, NativeFileMode.Open, NativeFileAccess.Read);

            BitmapDecoder bitmapDecoder = new BitmapDecoder(imagingFactory, fileStream, DecodeOptions.CacheOnDemand);

            BitmapFrameDecode frame = bitmapDecoder.GetFrame(0);

            FormatConverter converter = new FormatConverter(imagingFactory);
            converter.Initialize(frame, SharpDX.WIC.PixelFormat.Format32bppPRGBA);

            newBitmap = Bitmap1.FromWicBitmap(context, converter);

            Utilities.Dispose(ref bitmapDecoder);
            Utilities.Dispose(ref fileStream);
            Utilities.Dispose(ref imagingFactory);
            Utilities.Dispose(ref converter);
            Utilities.Dispose(ref frame);
            return newBitmap;
        }

        private SharpDX.Direct2D1.Bitmap LoadBitmapFromContentFile(SharpDX.Direct2D1.RenderTarget target, string filePath)
        {
            SharpDX.Direct2D1.Bitmap newBitmap;

            ImagingFactory imagingFactory = new ImagingFactory();
            NativeFileStream fileStream = new NativeFileStream(filePath, NativeFileMode.Open, NativeFileAccess.Read);

            BitmapDecoder bitmapDecoder = new BitmapDecoder(imagingFactory, fileStream, DecodeOptions.CacheOnDemand);

            BitmapFrameDecode frame = bitmapDecoder.GetFrame(0);

            FormatConverter converter = new FormatConverter(imagingFactory);
            converter.Initialize(frame, SharpDX.WIC.PixelFormat.Format32bppPRGBA);

            newBitmap = SharpDX.Direct2D1.Bitmap.FromWicBitmap(target, converter);

            Utilities.Dispose(ref bitmapDecoder);
            Utilities.Dispose(ref fileStream);
            Utilities.Dispose(ref imagingFactory);
            Utilities.Dispose(ref converter);
            Utilities.Dispose(ref frame);
            return newBitmap;
        }

        public void MouseMove(object sender, MouseHook.MouseMoveArgs e)
        {
            mouseX = e.Pos.x;
        }

        public void HandleExit()
        {
            cancelRendering.Cancel();

            cancelRendering.Dispose();
            bgSwapChain.Dispose();
            bgTarget.Dispose();
            fgTarget.Dispose();
            bgContext.Dispose();
            device.Dispose();
            bgDevice.Dispose();

            bg.Close();
            MouseHook.Stop();

            SendMessage(progman, 0x0034, new IntPtr(4), IntPtr.Zero);
            InvalidateRect(IntPtr.Zero, IntPtr.Zero, false);
        }

        protected override void OnExit(ExitEventArgs e)
        {
            HandleExit();
        }

        public void ApplicationExit(object sender, ExitEventArgs e)
        {
            HandleExit();
        }
    }
}
